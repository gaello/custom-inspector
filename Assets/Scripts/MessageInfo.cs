﻿using UnityEngine;

/// <summary>
/// Component containing message which will be displayed in Editor.
/// </summary>
public class MessageInfo : MonoBehaviour
{
    // Message value which will be displayed.
    public string message;
}
