# Custom Inspector in Unity

So you are looking for information on how to create custom inspector for your component? That's easy! This repository contain what you looking for!

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/05/16/custom-inspector-in-unity/

Enjoy!

---

# How to use it?

This repository contains an example of how you can create Custom Inspector in Unity.

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/custom-inspector/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

#Well done!

You have just learned how to create custom inspectors in Unity!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com
